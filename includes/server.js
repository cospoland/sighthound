'use strict'
const argv              = require('minimist')(process.argv.slice(2))
const winston           = require('winston')
const express           = require('express')
const yaml              = require('js-yaml')
const winstonMiddleware = require('express-winston')
const session           = require('express-session')
const routes            = require('./routing.js')
const fs                = require('fs')
let config              = {}
winston.level           = argv['loglevel'] || 'info'

if(argv['help']) {
    console.log(` Sighthound
 --loglevel debug|silly|info|error  sets initial log level`)
    return
}

try {
    config = yaml.safeLoad(fs.readFileSync('config.yml', 'utf8'));
} catch (e) {
    winston.log('error', 'Could not load Sighthound configuration file.')
    return winston.log('debug', e)
}

winston.level = argv['loglevel'] || config.LogLevel

const app = express()

app.use(winstonMiddleware.logger({
        transports: [
            new winston.transports.Console({
                json: false,
                colorize: true
            })
        ],
        meta: winston.level == "debug" ? true : false,
        msg: '[{{req.connection.remoteAddress}}] HTTP {{req.method}} {{res.statusCode}} {{req.url}} (User-Agent: {{req.headers["user-agent"]}}) {{res.responseTime}}ms',
        expressFormat: false,
        colorize: true
    })
)

winston.log('silly', 'set view engine')
app.set('view engine', 'pug')

winston.log('silly', 'session middleware')
app.use(session({
  secret: config.Site.Cookies.Secret,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: config.Site.Cookies.Secure }
}))

winston.log('silly', 'set trust proxy')
app.set('trust proxy', (ip) => {
  if (config.Server['Trust-IP'].contains(ip)) return true
  else return false
})

winston.log('silly', 'i18n load')
const sitelang = require(`../i18n/${config.Site.Language}.json`)

winston.log('silly', 'creating oauth2 client')
const oauth2 = require('simple-oauth2').create(config.Discord.OAuth2)

winston.log('silly', 'routes')
routes(app, config, winston, sitelang, oauth2)

winston.log('silly', 'listen')
app.listen(config.Site.Server.Port, () => {
    winston.log('info', `Started Sighthound's HTTP server. Listening on ${config.Site.Server.Port}.`)
})

let bot
function BotMgrCallback (inst) {
    bot = inst
    winston.log('info', 'Calling bot manager back...')
    bot.Run()
}
new (require('./bot.js'))(config, winston, BotMgrCallback)