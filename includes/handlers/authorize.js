'use strict'
module.exports = (req, res, config, winston, lang, oauth2) => {
    if (typeof req.query.code === 'undefined') {
        res.status(400).send('<strong>code</strong> query parameter required.')
        return
    }

    let tokenConfig = {
        code: req.query.code,
        redirect_uri: config.Discord.RedirectUri
    }

    oauth2.authorizationCode.getToken(tokenConfig, (error, result) => {
        if (error) {
            winston.log('error', `access token error: ${error.message}`)
            res.status(500).send('Discord API returned error. Please try again later.')
            return
        }
        let token = oauth2.accessToken.create(result)
        req.session.discord_token = token
        res.redirect('/manage')
    })
}