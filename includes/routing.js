'use strict'
const express  = require('express')
const overview = require('./handlers/overview.js')
const manage = require('./handlers/manage.js')
const authorize = require('./handlers/authorize.js')
module.exports = (app, config, winston, lang, oauth2) => {
    app.use('/static', express.static('static'))
    winston.log('silly', 'route: GET / -> appinfo')
    app.get('/', (req, res) => {
        overview(req, res, config, winston, lang)
    })
    app.get('/manage', (req, res) => {
        manage(req, res, config, winston, lang)
    })
    app.get('/callback', (req, res) => {
        authorize(req, res, config, winston, lang, oauth2)
    })
}