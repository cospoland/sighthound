'use strict'
module.exports = (req, res, config, winston, lang) => {
    res.render('page', {
        i18n:          lang,
        site:          config.Shared,
        active:        {
            overview: true,
            schedjob: false,
            schedreplace: false,
            schedimport: false
        },
        PageI18NKey:   "AppName",
        ActionI18NKey: "SchedJob",
        layout:        {
            intro: true
        }
    })
}