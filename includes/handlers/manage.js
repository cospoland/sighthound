'use strict'
module.exports = (req, res, config, winston, lang) => {
    res.render('page', {
        i18n:            lang,
        site:            config.Shared,
        active:          {
            overview:     true,
            schedjob:     false,
            schedreplace: false,
            schedimport:  false
        },
        PageI18NKey:     "ManageBots",
        ActionI18NKey:   "Appslist",
        PageDescI18NKey: "ManageBotsDesc",
        HasAction:       false,
        layout:          {
            intro:        false,
            controlpanel: true
        }
    })
}