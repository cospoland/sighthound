const fs = require('fs')

class SighthoundBotManager {
    constructor (config, winston, callback) {
        this.AppConfig = config
        this.Winston = winston
        this.Modules = {}
        this.i18n = {}

        winston.log('info', 'Bot manager is initializing...')
        
        winston.log('silly', 'loading i18n')
        let i18npath = `${__dirname}/../i18n`
        fs.readdir(i18npath, (err, files) => {
            if (err) {
                winston.log('error', 'Could not load translations.')
                winston.log('verbose', err)
                process.exit(1)
                return
            }
            files.forEach((file) => {
                if (!file.endsWith('.json')) return
                this.i18n[file.substring(0, file.length - 5)] = require(`${i18npath}/${file}`)
            })
            winston.log('silly', 'loading modules')
            config.Modules.forEach((ModuleName) => {
                let ModPath = `${__dirname}/../modules/${ModuleName}`
                this.Modules[ModuleName] = require(`${ModPath}/module.json`)
                this.Modules[ModuleName].I18N = {}
                let i18npath = `${ModPath}/${this.Modules[ModuleName].I18NDirectory}`
                let i18nl = fs.readdirSync(i18npath)
                i18nl.forEach((file) => {
                    if (!file.endsWith('.json')) return
                    this.Modules[ModuleName].I18N[file.substring(0, file.length - 5)] = require(`${i18npath}/${file}`)
                })
                this.Modules[ModuleName].EntrypointCode = require(`${ModPath}/${this.Modules[ModuleName].Entrypoint}`)
            })
            winston.log('info', 'Bot manager initialized. Returning appropate signal...')
            callback(this)
        })
    }

    Run () {
        this.Winston.log('info', 'Calling all modules...')
        
    }
}
module.exports = SighthoundBotManager